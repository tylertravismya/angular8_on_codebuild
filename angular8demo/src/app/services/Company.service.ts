import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Company} from '../models/Company';
import {EmployeeService} from '../services/Employee.service';
import {AddressService} from '../services/Address.service';
import { HelperBaseService } from './helperbase.service';

@Injectable()
export class CompanyService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	company : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Company 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addCompany(name, Employees, Address, Type) : Promise<any> {
    	const uri = this.ormUrl + '/Company/add';
    	const obj = {
      		name: name,
      		Employees: Employees != null && Employees.length > 0 ? Employees : null,
      		Address: Address != null && Address.length > 0 ? Address : null,
			Type: Type
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Company 
	// returns the results untouched as JSON representation of an
	// array of Company models
	// delegates via URI to an ORM handler
	//********************************************************************
	getCompanys() {
    	const uri = this.ormUrl + '/Company';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Company 
	// returns the results untouched as a JSON representation of a
	// Company model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editCompany(id) {
    	const uri = this.ormUrl + '/Company/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Company 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateCompany(name, Employees, Address, Type, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Company/update/' + id;
    	const obj = {
      		name: name,
      		Employees: Employees != null && Employees.length > 0 ? Employees : null,
      		Address: Address != null && Address.length > 0 ? Address : null,
			Type: Type
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Company 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteCompany(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Company/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a Address on a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignAddress( companyId, _addressId ): Promise<any> {

		// get the Company from storage
		this.loadHelper( companyId );
		
		// get the Address from storage
		var tmp 	= new AddressService(this.http).editAddress(_addressId);
		
		// assign the Address		
		this.company.address = tmp;
      		
		// save the Company
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Address on a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignAddress( companyId ): Promise<any> {

		// get the Company from storage
        this.loadHelper( companyId );
		
		// assign Address to null		
		this.company.address = null;
      		
		// save the Company
		return this.saveHelper();
	}
	

	//********************************************************************
	// adds one or more employeesIds as a Employees 
	// to a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addEmployees( companyId, employeesIds ): Promise<any> {

		// get the Company
		this.loadHelper( companyId );
				
		// split on a comma with no spaces
		var idList = employeesIds.split(',')

		// iterate over array of employees ids
		idList.forEach(function (id) {
			// read the Employee		
			var employee = new EmployeeService(this.http).editEmployee(id);	
			// add the Employee if not already assigned
			if ( this.company.employees.indexOf(employee) == -1 )
				this.company.employees.push(employee);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more employeesIds as a Employees 
	// from a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeEmployees( companyId, employeesIds ): Promise<any> {
		
		// get the Company
		this.loadHelper( companyId );

				
		// split on a comma with no spaces
		var idList 					= employeesIds.split(',');
		var employees 	= this.company.employees;
		
		if ( employees != null && employeesIds != null ) {
		
			// iterate over array of employees ids
			employees.forEach(function (obj) {				
				if ( employeesIds.indexOf(obj._id) > -1 ) {
					 // remove the Employee
					this.company.employees.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a Company
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Company/update/' + this.company._id;		
		
    	return this
      			.http
      			.post(uri, this.company)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Company
	//********************************************************************	
	loadHelper( id ) {
		this.editCompany(id)
        		.subscribe(res => {
        			this.company = res;
      			});
	}
}